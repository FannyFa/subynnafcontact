var app = require('express')();
var cors = require('cors');
var http = require('http').createServer(app);
var bodyParser = require('body-parser');
var axios = require('axios');
var config = require('./config.json').development;
app.use(cors());
app.use(bodyParser());


app.listen(config.port.http, function () {
  console.log(`Server http started on port : ${config.port.http}`)
})
